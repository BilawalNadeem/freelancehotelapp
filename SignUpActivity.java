package com.example.bilawal.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity {

    EditText name, email, password, cellno, address;
    Button btn;
    String nme,eml,pwd,clno,add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        name = (EditText) findViewById(R.id.signupview1);
        email = (EditText) findViewById(R.id.signupview2);
        password = (EditText) findViewById(R.id.signupview3);
        cellno = (EditText) findViewById(R.id.signupview4);
        address = (EditText) findViewById(R.id.signupview5);
        btn = (Button) findViewById(R.id.btnsignup);
        //OnClickButtonListener();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vv) {
                if (!validationemail(email.getText().toString())) {
                    email.setError("Invalid Email");
                    email.requestFocus();
                } else if (!validationpassword(password.getText().toString())) {
                    password.setError("enter minimum 6 digit password");
                } else if (!validatecellno(cellno.getText().toString())) {
                    cellno.setError("Please Write a complete cellphone number");
                } else if (!validateaddress(address.getText().toString())) {
                    address.setError("Please enter the Adrress");
                } else if (!validatename(name.getText().toString())) {
                    name.setError("Please write the Name");
                } else {
                    Toast.makeText(SignUpActivity.this, "Input Validation Successfull", Toast.LENGTH_LONG).show();

                    OnClickButtonListener();
                }
            }
        });
    }


    public void userReg(View view) {

        nme= name.getText().toString();
        eml = email.getText().toString();
        pwd = password.getText().toString();
        clno = cellno.getText().toString();
        add = address.getText().toString();

        String method = "register";
        BackgroundTask backgroundTask = new BackgroundTask(this);
        backgroundTask.execute(method, nme, eml,pwd,clno,add);
        finish();
    }


    protected boolean validatecellno(String cellno) {
    if(cellno!=null && cellno.length()==11)
    {
        return true;
    }
        else{

        return false;
    }
    }

    protected boolean validateaddress(String address) {
    if(address!="")
    {
        return true;
    }
        else{
        return false;
    }
    }
    protected boolean validatename(String name) {
    if(name!="")
    {
        return true;
    }
        else{
        return false;
    }

    }

    public void OnClickButtonListener()
    {
        btn=(Button)findViewById(R.id.btnsignup);
        btn.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                        Intent intent =new Intent("com.example.bilawal.myapplication.SigninActivity");
                        startActivity(intent);

                    }
                }

        );


    }

/*

password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
    public void onFocusChange(View v, boolean hasFocus) {
        if (password.getText().length() < 5) {
            password.setError("Please Enter 6 digit password");

        }

    }


});
    email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

        }
    });

    }

*/

    protected boolean validationemail(String email) {
        String emailpattern = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
        Pattern pattern = Pattern.compile(emailpattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }


    protected boolean validationpassword(String password) {
        if (password != null && password.length() >= 6) {
                return true;

        } else {
            return false;
        }

    }}



   /*

*/


