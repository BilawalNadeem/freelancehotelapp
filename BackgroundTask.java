package com.example.bilawal.myapplication;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Bilawal on 2016-07-15.
 */
public class BackgroundTask extends AsyncTask <String,Void,String> {


    Context ctx;
    BackgroundTask(Context ctx)
    {
        this.ctx=ctx;
    }
    protected void onPreExecute()
    {
super.onPreExecute();
    }

    protected String doInBackground(String...params)
    {
String regurl="http://192.168.210.1:3306/B:phpandroid/register.php";
        String method=params[0];
if(method.equals("register")){

    String nme=params[1];
    String eml=params[2];
    String pwd=params[3];
    String clno=params[4];
    String add=params[5];

    try{
        URL url =new URL(regurl);


        HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setDoOutput(true);
        OutputStream OS = httpURLConnection.getOutputStream();
        BufferedWriter bufferedWriter= new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));

        String data= URLEncoder.encode("nme", "UTF-8")+ "=" +URLEncoder.encode(nme,"UTF-8")+ "&" +
                URLEncoder.encode("eml","UTF-8")+ "=" +URLEncoder.encode(eml,"UTF-8")+ "&" +
                URLEncoder.encode("pwd","UTF-8")+ "=" +URLEncoder.encode(pwd,"UTF-8")+ "&" +
                URLEncoder.encode("clno","UTF-8")+ "=" +URLEncoder.encode(clno,"UTF-8")+ "&" +
                URLEncoder.encode("add","UTF-8")+ "=" +URLEncoder.encode(add,"UTF-8");


        bufferedWriter.write(data);
        bufferedWriter.flush();
        bufferedWriter.close();
        OS.close();
        InputStream IS=httpURLConnection.getInputStream();
        IS.close();
        return "Registration Success";

    }

    catch (MalformedURLException e) {
        e.printStackTrace();
    }

    catch (IOException e) {
        e.printStackTrace();
    }

}
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
    }
}



