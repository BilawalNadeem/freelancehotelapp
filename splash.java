package com.example.bilawal.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Bilawal on 2016-07-01.
 */
public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
   setContentView(R.layout.splash_screen);
    Thread myThread=new Thread()
    {
        public void run()
        {
            try {
                sleep(6000);
                Intent startMainScreen = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(startMainScreen);
                 finish();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };
        myThread.start();

    }
}
